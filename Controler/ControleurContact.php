<?php

require_once 'Model/Contact.php';
require_once 'Vue/Vue.php';

class ControleurContact {

    private $contact;

    public function __construct() {
        $this->contact = new Contact();
    }

    // Affiche les détails sur un contact
    public function detailContact($idContact) {
        $contact = $this->contact->getContact($idContact);
        $vue = new Vue("ContactDetail");
        $vue->generer(array('contact' => $contact));
    }

    // Affiche les détails sur un billet
    public function newContact() {
        $vue = new Vue("ContactCreate");
        $vue->generer(array());
    }

    // Ajoute un commentaire à un billet
    public function createContact($nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays) {
        // Sauvegarde du commentaire
        $idContact = $this->contact->ajouterContact($nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays);
        // Actualisation de l'affichage du billet
        $this->detailContact($idContact);
    }

    public function updateContact($id, $nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays) {
        // Sauvegarde du commentaire
        $idContact = $this->contact->majContact($id, $nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays);
        // Actualisation de l'affichage du billet
        $this->detailContact($idContact);
    }

    public function deleteContact($idContact) {
        $idContact = $this->contact->supprContact($idContact);
        $contacts = $this->contact->getContacts();
        $vue = new Vue("Accueil");
        $vue->generer(array('contacts' => $contacts));
    }

}
