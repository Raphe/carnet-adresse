<?php

require_once 'Controler/ControleurAccueil.php';
require_once 'Controler/ControleurContact.php';
require_once 'Vue/Vue.php';
class Routeur {

    private $ctrlAccueil;
    private $ctrlContact;

    public function __construct() {
        $this->ctrlAccueil = new ControleurAccueil();
        $this->ctrlContact = new ControleurContact();
    }

    // Route une requête entrante : exécution l'action associée
    public function routerRequete() {
        try {
            if (isset($_GET['action'])) {
                if ($_GET['action'] == 'detail') {
                    $idContact = intval($this->getParametre($_GET, 'id'));
                    if ($idContact != 0) {
                        $this->ctrlContact->detailContact($idContact);
                    }
                    else
                        throw new Exception("Identifiant de contact non valide");
                }else if($_GET['action'] == 'new'){
                    $this->ctrlContact->newContact();
                }  else if ($_GET['action'] == 'create' || $_GET['action'] == 'update') {
                      $nom = $this->getParametre($_POST, 'nom');
                      $prenom = $this->getParametre($_POST, 'prenom');
                      $tel = $this->getParametre($_POST, 'tel');
                      $mail = $this->getParametre($_POST, 'mail');
                      $adresse_l1 = $this->getParametre($_POST, 'adresse_l1');
                      $adresse_l2 = $this->getParametre($_POST, 'adresse_l2');
                      $cp = $this->getParametre($_POST, 'cp');
                      $ville = $this->getParametre($_POST, 'ville');
                      $pays = $this->getParametre($_POST, 'pays');
                      if($_GET['action'] == 'create'){
                          $this->ctrlContact->createContact($nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays);
                      }else if ($_GET['action'] == 'update'){
                        $id = $this->getParametre($_POST, 'id');
                        $this->ctrlContact->updateContact($id, $nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays);
                      }
                  }else if($_GET['action'] == 'delete'){
                    $idContact = intval($this->getParametre($_GET, 'id'));
                    if ($idContact != 0) {
                        $this->ctrlContact->deleteContact($idContact);
                    }
                    else
                        throw new Exception("Identifiant de contact non valide");
                  }
                else
                    throw new Exception("Action non valide");
            }
            else {  // aucune action définie : affichage de l'accueil
                $this->ctrlAccueil->accueil();
            }
        }
        catch (Exception $e) {
            $this->erreur($e->getMessage());
        }
    }

    // Affiche une erreur
    private function erreur($msgErreur) {
        $vue = new Vue("Erreur");
        $vue->generer(array('msgErreur' => $msgErreur));
    }

    // Recherche un paramètre dans un tableau
    private function getParametre($tableau, $nom) {
        if (isset($tableau[$nom])) {
            return $tableau[$nom];
        }
        else
            throw new Exception("Paramètre '$nom' absent");
    }

}
