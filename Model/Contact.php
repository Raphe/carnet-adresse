<?php

require_once 'Model/Modele.php';

/**
 * Fournit les services d'accès aux genres musicaux
 *
 * @author Baptiste Pesquet
 */
class Contact extends Modele {

    /** Renvoie la liste des billets du blog
     *
     * @return PDOStatement La liste des billets
     */
    public function getContacts() {
        $sql = 'select Id_contact as id,'
                . ' Nom_contact as nom,'
                . ' Prenom_contact as prenom,'
                . ' Tel_contact as tel,'
                . ' Mail_contact as mail,'
                . ' Adresse_l1_contact as adresse_l1,'
                . ' Adresse_l2_contact as adresse_l2,'
                . ' CP_contact as cp,'
                . ' Ville_contact as ville,'
                . ' Pays_contact as pays'
                . ' FROM contact'
                . ' order by Nom_contact ASC';
        $contacts = $this->executerRequete($sql);
        return $contacts;
    }

    /** Renvoie les informations sur un billet
     *
     * @param int $id L'identifiant du billet
     * @return array Le billet
     * @throws Exception Si l'identifiant du billet est inconnu
     */
    public function getContact($idContact) {
        $sql = 'select Id_contact as id,'
                . ' Nom_contact as nom,'
                . ' Prenom_contact as prenom,'
                . ' Tel_contact as tel,'
                . ' Mail_contact as mail,'
                . ' Adresse_l1_contact as adresse_l1,'
                . ' Adresse_l2_contact as adresse_l2,'
                . ' CP_contact as cp,'
                . ' Ville_contact as ville,'
                . ' Pays_contact as pays'
                . ' FROM contact'
                . ' where Id_contact=?';
        $contact = $this->executerRequete($sql, array($idContact));
        if ($contact->rowCount() > 0)
            return $contact->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun contact ne correspond à l'identifiant '$idContact'");
    }

    // Ajoute un contact dans la base
    public function ajouterContact($nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays) {
        $sql = 'INSERT INTO contact(Nom_contact, Prenom_contact, Tel_Contact, Mail_Contact, Adresse_l1_contact, Adresse_l2_contact, CP_contact, Ville_contact, Pays_contact)'
        . ' values(?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->executerRequete($sql, array($nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays));
        $idContact = $this->lastInsertId();
        return $idContact;
    }

    // Ajoute un contact dans la base
    public function majContact($id, $nom, $prenom, $tel, $mail, $adresse_l1, $adresse_l2, $cp, $ville, $pays) {
        $sql = 'UPDATE contact SET Nom_contact = :nom, Prenom_contact = :prenom, Tel_Contact = :tel, Mail_Contact = :mail, Adresse_l1_contact = :adresse_l1, Adresse_l2_contact = :adresse_l2, CP_contact = :cp, Ville_contact = :ville, Pays_contact = :pays'
            . ' WHERE Id_contact = :id';
        $this->executerRequete($sql, array(
                                    'nom' => $nom,
                                    'prenom' => $prenom,
                                    'tel' => $tel,
                                    'mail' => $mail,
                                    'adresse_l1' => $adresse_l1,
                                    'adresse_l2' => $adresse_l2,
                                    'cp' => $cp,
                                    'ville' => $ville,
                                    'pays' => $pays,
                                    'id' => intval($id))
                                );
        return $id;
    }

    public function supprContact($id) {
        $sql = 'DELETE FROM contact WHERE Id_contact = :id';
        $this->executerRequete($sql, array('id' => intval($id)));
    }

}
