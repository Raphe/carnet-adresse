<?php

require_once 'Model/Contact.php';
require_once 'Vue/Vue.php';

class ControleurAccueil {

    private $contact;

    public function __construct() {
        $this->contact = new Contact();
    }

// Affiche la liste de tous les billets du blog
    public function accueil() {
        $contacts = $this->contact->getContacts();
        $vue = new Vue("Accueil");
        $vue->generer(array('contacts' => $contacts));
    }

}
