<?php $this->titre = "Mon carnet d'adresse"; ?>
<h2>Mes contacts</h2>
<?php foreach ($contacts as $contact):
    ?>
    <div class="contact">
        <h4><?php echo $contact['nom'].' '.$contact['prenom']; ?></h4>
        <div class="info">
          <span class="title"><i></i> Tel : </span>
          <?php echo $contact['tel']; ?>
        </div>

        <div class="info">
          <span class="title"><i></i> E-mail : </span>
          <?php echo $contact['mail']; ?>
        </div>

        <div class="info">
          <span class="title"><i></i> Adresse : </span>
          <?php echo '<br>'.$contact['adresse_l1'].$contact['adresse_l2'].'<br>'.$contact['cp'].'<br>'.$contact['ville'].'<br>'.$contact['pays']; ?>
        </div>

        <a href="<?= "index.php?action=detail&id=" . $contact['id'] ?>">
          Modifier
        </a>
        <br>
        <a href="<?= "index.php?action=delete&id=" . $contact['id'] ?>">
          Supprimer
        </a>
    </div>
    <hr />
<?php endforeach; ?>
<a href="index.php?action=new">Créer un nouveau contact</a>
