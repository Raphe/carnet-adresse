<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <!--===============================================================================================-->
        	<link rel="icon" type="image/png" href="Assets/images/icons/favicon.ico"/>
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/vendor/animate/animate.css">
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        	<link rel="stylesheet" type="text/css" href="Assets/css/util.css">
        	<link rel="stylesheet" type="text/css" href="Assets/css/main.css">
        <!--===============================================================================================-->

        <title><?= $titre ?></title>
    </head>
    <body>
        <div id="global">
          <div class="bg-contact2" style="background-image: url('Assets/images/bg-01.jpg');">
            <header>
                <a href="index.php"><h1 id="titreBlog">Mon carnet d'adresse</h1></a>
                <p>Avec toutes mes adresses.</p>
            </header>
            <div id="contenu">
                <?= $contenu ?>
            </div> <!-- #contenu -->
            <footer id="piedBlog">
                Blog réalisé avec PHP, HTML5 et CSS.
            </footer>
          </div>
        </div> <!-- #global -->

        <!--===============================================================================================-->
        	<script src="Assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        	<script src="Assets/vendor/bootstrap/js/popper.js"></script>
        	<script src="Assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        	<script src="Assets/vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        	<script src="Assets/js/main.js"></script>
    </body>
</html>
