-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 08 jan. 2018 à 14:03
-- Version du serveur :  5.7.19
-- Version de PHP :  7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carnet_adresse`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `Id_contact` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `Nom_contact` varchar(100) NOT NULL,
  `Prenom_contact` varchar(100) DEFAULT NULL,
  `Tel_contact` varchar(25) DEFAULT NULL,
  `Mail_contact` varchar(150) DEFAULT NULL,
  `Adresse_l1_contact` varchar(200) DEFAULT NULL,
  `Adresse_l2_contact` varchar(200) DEFAULT NULL,
  `CP_contact` varchar(10) DEFAULT NULL,
  `Ville_contact` varchar(150) DEFAULT NULL,
  `Pays_contact` int(150) DEFAULT NULL,
  PRIMARY KEY (`Id_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
