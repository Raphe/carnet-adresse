<?php $this->titre = "Mettre à jour un contact"; ?>
  <div class="container-contact2">
    <div class="wrap-contact2">
      <form class="contact2-form validate-form">
        <span class="contact2-form-title">
          <?php echo $contact['nom'].' '.$contact['prenom']; ?>
        </span>

        <div class="wrap-input2 validate-input" data-validate="Le nom est requis">
          <input class = "input2" id="nom" name="nom" type="text" placeholder="Nom" value="<?php echo $contact['nom']; ?>" required />
          <span class="focus-input2" data-placeholder="Nom"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="prenom" name="prenom" type="text" placeholder="prenom" value="<?php echo $contact['prenom']; ?>"/>
          <span class="focus-input2" data-placeholder="prenom"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="tel" name="tel" type="text" placeholder="tel" value="<?php echo $contact['tel']; ?>"/>
          <span class="focus-input2" data-placeholder="tel"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="mail" name="mail" type="text" placeholder="mail" value="<?php echo $contact['mail']; ?>"/>
          <span class="focus-input2" data-placeholder="mail"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="adresse_l1" name="adresse_l1" type="text" placeholder="adresse_l1" value="<?php echo $contact['adresse_l1']; ?>"/>
          <span class="focus-input2" data-placeholder="adresse_l1"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="adresse_l2" name="adresse_l2" type="text" placeholder="adresse_l2" value="<?php echo $contact['adresse_l2']; ?>"/>
          <span class="focus-input2" data-placeholder="adresse_l2"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="cp" name="cp" type="text" placeholder="cp" value="<?php echo $contact['cp']; ?>"/>
          <span class="focus-input2" data-placeholder="cp"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="ville" name="ville" type="text" placeholder="ville" value="<?php echo $contact['ville']; ?>"/>
          <span class="focus-input2" data-placeholder="ville"></span>
        </div>

        <div class="wrap-input2 validate-input">
          <input class = "input2" id="pays" name="pays" type="text" placeholder="pays" value="<?php echo $contact['pays']; ?>"/>
          <span class="focus-input2" data-placeholder="pays"></span>
        </div>


        <div class="container-contact2-form-btn">
          <div class="wrap-contact2-form-btn">
            <div class="contact2-form-bgbtn"></div>
            <input class="contact2-form-btn" type="submit" value="Mettre à jour" />
          </div>
        </div>
      </form>
    </div>
  </div>

<?php// $nom, $prenom, $tel, $adresse_l1, $adresse_l2, $cp, $ville, $pays ?>
